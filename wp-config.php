<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'investor_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hW)X__@TDz^/r6wttQMuxFM!EdqUr5u4=(Ye!u 7DfKNMtzZA?#[)m2zD3#gXm[]');
define('SECURE_AUTH_KEY',  't@x.{p&]s Amn<`.A_K3] 2TJ%vod-d~P>!]OGRnj>4yWOTd|-@0Q.Dh9ehX?YfO');
define('LOGGED_IN_KEY',    '5}p)QuIY1|:3>d4hF=7T~`8-Rww@!.iB=4W8u#KRw`/HT~WW(il&|9IfIfHM:,>x');
define('NONCE_KEY',        'hxE#KPq<gY>Yv!xusJ 2kLQN.]}3UAvGc-b|cou:X[23}hh]1g`kVpS4giq>HFP]');
define('AUTH_SALT',        '4JF4~KXmb Ot}~4Bqw)=hV!;thol+r:R13CX/3>gSN$9WpV7v+Z^QsEus!S[;mkV');
define('SECURE_AUTH_SALT', 'O9iuZ+!QnbBc^nT*>V,t=eqL[`j~w>Y1TjLom&#H->?/sb,#&~URRCc/HQu/,lB_');
define('LOGGED_IN_SALT',   '4O-<yI!{&J_EAJtla}yKqjz|6%/l{hRXjZ5`*z%Q>@V^t),CskM-(@Zw[Sf(=BIS');
define('NONCE_SALT',       'feSp{Z:,5 6G*=pMKE?hAt4xVxEyj~{em8b[8|&S73FYY)KCyQfU:#fkITNT)CQ_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
