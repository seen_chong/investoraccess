<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

        <link href='https://fonts.googleapis.com/css?family=Work+Sans:100,500,600,700,800' rel='stylesheet' type='text/css'>



		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/pagepiling/jquery.pagepiling.css" />
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/pagepiling/jquery.pagepiling.js"></script>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        // conditionizr.config({
        //     assets: '<?php echo get_template_directory_uri(); ?>',
        //     tests: {}
        // });
        </script>

	</head>
	<body>


<!-- 		<div class="wrapper"> -->


			<header class="header clear" role="banner">

				<div class="header-wrap">
					<div class="logo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/iap-logo4.png" alt="Logo" class="logo-img">
					</div>



					<nav class="nav" role="navigation">
						<ul id="nav-menu">
                    		<li data-menuanchor="section1" id="story-section">
                        		<a href="#section1">Overview</a>
                    		</li>
                    		<li data-menuanchor="section2" id="overview-section">
                        		<a href="#section2">Analysis</a>
                    		</li>
                    		<li data-menuanchor="section3" id="dev-section">
                        		<a href="#section3">Process</a>
                    		</li>
                    		<li data-menuanchor="section4" id="dev-section">
                        		<a href="#section4">Who We Are</a>
                    		</li>
                    		<li data-menuanchor="section5" id="dev-section">
                        		<a href="#section5">Team</a>
                    		</li>
                    		<li data-menuanchor="contact" id="dev-section">
                        		<a href="#contact">Contact</a>
                    		</li>
                		</ul>
					</nav>
				</div>

			</header>

