<?php get_header(); ?>

<div>
<div id="pagepiling">


    <div class="section" id="section1">
    	<div class="section-text">
    		<h3><?php the_field('overview_title'); ?></h3>
    		<p><?php the_field('overview_text'); ?></p>
    	</div>
    	
    </div>

    <div class="section" id="section2" style="background-image:url(<?php the_field('section_2_background'); ?>">
    	<div class="section-text">
    		<h3><?php the_field('section_2_title'); ?></h3>
    		<p><?php the_field('section_2_text'); ?></p>
    	</div>
    </div>

    <div class="section" id="section3" style="background-image:url(<?php the_field('section_3_background'); ?>">
    	<div class="section-text">
    		<h3><?php the_field('section_3_title'); ?></h3>
    		<p><?php the_field('section_3_text'); ?></p>
    	</div>
<!--     	<div class="section-nose"></div>
    	<div class="section-frown">
    		<img src="<?php echo get_template_directory_uri(); ?>/img/frown.png">
    	</div> -->
<!--     	<div class="section-mini-text">
    		<h5>Know How To Answer Questions Like</h5>
    		<img src="<?php echo get_template_directory_uri(); ?>/img/iap-logo2.png">
    		<p>Why is the team uniquely capable to execute the company's business plan?</p>
    	</div>
 -->    </div>

    <div class="section" id="section4" style="background-image:url(<?php the_field('team_background'); ?>">
    	<div class="section-header" id="desktop">
	    	<img src="<?php echo get_template_directory_uri(); ?>/img/iap-logo4.png">
    	</div>
    	<div class="section-header" id="mobile">
    		<h3>Investor Access Partners</h3>
    	</div>
    	<h2>is</h2>
     	<div class="section-text">
    		<p><?php the_field('team_text'); ?></p>
    	</div>
    </div>

    <div class="section" id="section5" style="background-image:url(<?php the_field('contact_background'); ?>">
    	<div class="section-header">
            <img src="">
	    	<h1><?php the_field('contact_title'); ?></h1>
    	</div>

<!--     	<div class="section-text">
    		<p><//?php the_field('contact_text'); ?></p>
    	</div> -->

    	<div class="section-meet">
    		<img class="headshot" src="<?php the_field('employee_1_image'); ?>">
    		<div class="section-bio">
    			<h3><?php the_field('employee_1_name'); ?></h3>
    			<p><?php the_field('employee_1_bio'); ?></p>
    			<a class="call" href="">
    				<button>
    					<img class="phone-icon" src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.png">
    					Call Kit
    				</button>
    			</a>
    		</div>
    	</div>
    </div>




    <div class="section" id="contact" style="background-image:url(<?php the_field('contact_background'); ?>">
    	<div class="contact-form">
    		<h2>77 West 55th Street, Suite 1900 &bull; New York, NY  10019</h2>
    		<div class="contact-wrapper">
    			<?php echo do_shortcode('[contact-form-7 id="9" title="Contact Form"]'); ?>
    		</div>
    	</div>
    </div>

</div>



</div>


<?php get_footer(); ?>

<script type="text/javascript">
$(document).ready(function() {
    $('#pagepiling').pagepiling({
        menu: '#nav-menu',
        direction: 'vertical',
        verticalCentered: true,
        sectionsColor: [],
        anchors: ['section1', 'section2', 'section3', 'section4', 'section5', 'contact'],
        scrollingSpeed: 700,
        easing: 'swing',
        loopBottom: false,
        loopTop: false,
        css3: true,
        navigation: {
            'textColor': '#fff',
            'bulletsColor': '#fff',
            'position': 'right'
        },
        normalScrollElements: null,
        normalScrollElementTouchThreshold: 5,
        touchSensitivity: 5,
        keyboardScrolling: true,
        sectionSelector: '.section',
        animateAnchor: false,

        //events
        onLeave: function(index, nextIndex, direction){},
        afterLoad: function(anchorLink, index){},
        afterRender: function(){},
    });
});
</script>
